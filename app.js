const express = require('express') // import express
const app = express() // This line creates an instance of Express app
//  allowing you to define routes and handle
const port = 8080

app.get('/',(req, res) => {
    res.send("This is my Node Application for CICD Pipeline")
})

app.listen(port ,() => {
    console.log(`Application is listening at port https://localhost:${port}`)
})

